variable "prod_auth0_domain" {}
variable "prod_auth0_client_id" {}
variable "prod_auth0_client_secret" {}
variable "prod_project_name" {
    type = string
}

