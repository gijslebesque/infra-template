# Infra

This directory provision an AWS S3 bucket which stores the front end code.
A CloudFront resource to deliver the content of the aforementioned bucket.

It also creates an SPA application on Auth0 to be used for authentication.

Finally it provisions an IAM user that has the correct rights to manage these resources.

The API is organised by Serverless. In order for Serverless to do its job you need a IAM role,
this is also set in this folder.

**IMPORTANT: Do these steps per environment that you want to setup.**.

## Requirements

First setup a project name and put it in your environment variables.

```
$ TF_VAR_[THE_ENVIRONMENT]_project_name: your-project-name <- needs to be unique, also per environment
```

**IMPORTANT: Between the square brackets you have to define the environment, for example for prod:**.

```
$ export TF_VAR_prod_project_name='my-new-project-prod'
```

### AWS

Make sure you have an AWS account and add the API keys that are generated for the root user in your environment variables.

```
$ export AWS_ACCESS_KEY_ID={AWS_ACCESS_KEY_ID}
$ export AWS_SECRET_ACCESS_KEY={AWS_SECRET_ACCESS_KEY}
```

Go to https://console.aws.amazon.com/iam/home?region=us-east-1#/users and create a new user with only Programmatic Access named the same as your project name mentioned above. Don’t give the user any permissions, terraform handles this. Download the access keys you will need them later to grant Gitlab access.

### Auth0

Create a new tenant.
Go to the Applications section of your Auth0 Dashboard and create a new application with a new you like.
The type of the application should be "Machine to Machine Application".
Select it in the dropdown and then authorize all scopes by clicking "All" in the top right of the scopes selection area.
Now you receive the information to create applications with terraform.

```
$ export TF_VAR[THE_ENVIRONMENT]_auth0_domain={AUTH0_DOMAIN}
$ export TF_VAR[THE_ENVIRONMENT]_auth0_client_id={CLIENT_ID}
$ export TF_VAR[THE_ENVIRONMENT]_auth0_client_secret={CLIENT_SECRET}

```

### Terraform

`cd` into the infra and then the directory wish to privision (e.g prod or staging). Run `terraform init` and then `terraform apply`

### Serverless

Create a .env file similar to the .env.example in the API directory. Populate the environment variables with
the Auth0 tenant name created earlier and run `serverless deploy`.

You can find the API urls in AWS console under the output tab in CloudFormation.

### Gitlab

On Gitlab you will need to set the AWS API keys that you have obtained from creating an IAM user in the AWS console.

These are:

```
AWS_ACCESS_KEY_ID_STAGING={your-staging-id}
AWS_SECRET_ACCESS_KEY_STAGING={your-staging-access-key}
AWS_ACCESS_KEY_ID_PROD={your-prod-id}
AWS_SECRET_ACCESS_KEY_PROD={your-prod-access-key}
```

In the .gitlab-ci.yml you'll need to specify the location of the S3 bucket that the front-end is located.
You can find the names of the bucket in the AWS console.

# Resources

https://medium.com/geekculture/serve-your-react-app-with-aws-cloudfront-using-gitlab-and-terraform-322b2526943e
