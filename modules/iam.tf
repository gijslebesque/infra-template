
# Retrieve gitlab-user as a resource
# You have to manually create this user in the AWS console.
# Don't give the user any permissions 
data "aws_iam_user" "user" {
  user_name = "${var.project_name}"
}

# Create the policy to access the S3 bucket
resource "aws_iam_policy" "ci_policy" {
  name        = "${var.project_name}-gitlab-ci-policy"
  path        = "/"
  description = "Gitlab CI policy"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [

      #s3 policy for front end bucket
      {
        Action = [
          "s3:PutObject",
          "s3:PutObjectAcl",
           "s3:ListBucket"
        ],
        Effect = "Allow",
        Resource = [
          "${aws_s3_bucket.static_react_bucket.arn}/*"
        ]
      },
      #lambda policy
      {
        Effect    = "Allow",
        Resource = ["arn:aws:logs:*:*:log-group:/aws/lambda/*"],
        Action = [
            "logs:DescribeLogStreams",
            "logs:GetLogEvents",
            "logs:FilterLogEvents",
          ]
      },
      {
        Effect    = "Allow",
        Resource = ["*"],
        Action   = ["iam:PassRole"]
      },

      {

          Effect    = "Allow",
          Resource = ["*"],
          Action = [
          "cloudformation:*",
          "s3:*",
          "cloudwatch:*",
          "ec2:DescribeSecurityGroups",
          "ec2:DescribeSubnets",
          "ec2:DescribeVpcs",
          "kms:ListAliases",
          "iam:*",
          "lambda:*",
          "logs:*",
          "apigateway:*",
          "states:DescribeStateMachine",
          "states:ListStateMachines",
          "tag:GetResources",
          "xray:GetTraceSummaries",
          "xray:BatchGetTraces",
          ]
      }
    ]
  })
}

# Attach the policy to our user
resource "aws_iam_policy_attachment" "gitlab_ci_attachment" {
  name       = "gitlab-ci-attachment"
  users      = [data.aws_iam_user.user.user_name]
  policy_arn = aws_iam_policy.ci_policy.arn
}