# The variable blocks create the variable reference that the Provider configuration uses. 
# You don't need to set a variable default value in the config file itself; 
# Terraform will fill them in from your TF_VAR_* environment variables.

variable "auth0_domain" {}
variable "auth0_client_id" {}
variable "auth0_client_secret" {}
variable "project_name" {
    type = string
}
