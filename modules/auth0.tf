
# For Terraform to be able to create Clients and APIs in Auth0,
# you'll need to manually create an Auth0 Machine-to-Machine Application 
# that allows Terraform to communicate with Auth0.

# https://auth0.com/blog/use-terraform-to-manage-your-auth0-configuration/

provider "auth0" {
  domain        = var.auth0_domain
  client_id     = var.auth0_client_id
  client_secret = var.auth0_client_secret
}

resource "auth0_client" "front-end" {
  name                = var.project_name
  description         = "App via Terraform"
  app_type            = "spa"
  token_endpoint_auth_method = "none"
  callbacks           = ["http://localhost:3000", "https://${aws_cloudfront_distribution.cf_distribution.domain_name}"]
  allowed_origins     = ["http://localhost:3000", "https://${aws_cloudfront_distribution.cf_distribution.domain_name}"]
  allowed_logout_urls = ["http://localhost:3000", "https://${aws_cloudfront_distribution.cf_distribution.domain_name}"]
  web_origins         = ["http://localhost:3000", "https://${aws_cloudfront_distribution.cf_distribution.domain_name}"]
  oidc_conformant     = true
  grant_types = ["authorization_code","implicit","refresh_token"]
  refresh_token {

    leeway = 0
    rotation_type = "non-rotating"
    token_lifetime = 84600
    expiration_type = "non-expiring"
  } 

  jwt_configuration {
    alg = "RS256"
    lifetime_in_seconds = 3600

  }
}