variable "staging_auth0_domain" {}
variable "staging_auth0_client_id" {}
variable "staging_auth0_client_secret" {}
variable "staging_project_name" {
    type = string
}
